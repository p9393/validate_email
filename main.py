from fastapi import FastAPI, Request
from valid_email import valid_email

app = FastAPI(
    title="validate email",
    description="Esta api valida el formato de emails que sean correctos",
    version="1.0"
)


@app.get("/")
def home():
    return {"Saludo": "Hola "}


@app.get("/validate")
def main(request: Request):
    try:
        email_list = request.query_params.get("emails", "")
        email_list = email_list.split(",")
        valid_emails = list(filter(lambda email: valid_email(email), email_list))
        return {"valid_email": valid_emails}
    except Exception as e:
        return {"error": e}
