import re


def valid_email(email: str):
    pattern = re.compile(r"^([a-zA-Z0-9_-])+@([a-zA-Z0-9])+\.([a-zA-Z]){1,3}$")
    return re.match(pattern, email)
